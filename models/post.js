var mongoose = require('mongoose');

var postSchema = mongoose.Schema({
	title: String,
	author: String,
	description: String,
	comments: [{ 
		body: String, 
		date: Date }],
  	date: { 
  		type: Date, 
  		default: Date.now },
});

module.exports = mongoose.model('Post', postSchema);