var req = require('request');


describe("routeAPI", function () {
        
        /*GET all Posts  */
        it("should return all the posts", function () {
        	req.get('http://localhost:3000/api/posts', function(err, res, body){
        		expect(res.send(200));
        	});
        });

        /*GET one Post by id  */
        it("should return one post", function(){
        	var id = 23456789; 
        	req.get('http://localhost:3000/api/posts/'+id, function(err, res, body){
        		expect(res.send(200));
        	});
        });

        /*POST a new Post  */
        it('should create a new post', function(){
        	var data = {
        		title:'some title',
        		author: 'some author',
        		date: Date.now
        	};
        	req.post('http://localhost:3000/create_post',data, function(err, res, body){
        		expect(res.send(200));
        	});
        });

        /*DELETE a Post by id */
        it('should delete a post', function(){
            var id = 876543;
            req.delete('http://localhost:3000/api/post/'+id,function(err, res, body){
                expect(res.send(200));
            });
        });

        /*Patch a Post to add a comment*/
        it('should add a comment', function(){
            var idPost = 876543;
            req.patch('http://localhost:3000/api/post/'+idPost,function(err, res, body){
                expect(res.send(200));
            });
        });

        /*DELETE a Comment */
        it('should delete| a comment', function(){
            var idPost = 239877612;
            var idComment = 63745845;
            req.delete('http://localhost:3000/api/post/'+idPost+'/'+idComment,function(err, res, body){
                expect(res.send(200));
            });
        });
});