var express = require('express');
var router = express.Router();
var Post = require('../models/post');

/* GET posts listing. */
router.get('/', function(req, res, next) {
  Post.find({}, function(err, data){
  	if(err){
  		return console.log(err);
  	}
  	res.json(data);
  });
});

/*GET only one post by id*/
router.get('/:id', function(req, res, next){
	Post.findById(req.params.id, function(err, data){
		if (err){
			return console.log(err);
		}
		if (data === null){
			res.send("Post with id: "+req.params.id+" does't exist!");
		}
		else{
				res.json(data);
		}
	});
});

/*DELETE a post by id*/
router.delete('/:id', function(req, res, next) {
	Post.findByIdAndRemove(req.params.id, function	(err, data){
		if (err){
			return console.log(err);
		}
		res.send({
			success: true,
			message: "Post with id: "+ req.params.id+ "succesfully deleted!"
		});
	});
});


/*PUT  an exisitng post (add commments)*/
router.patch('/:id', function(req, res, next){
	var comment = {
		body: req.body.comment,
		date: Date.now()
	};
	console.log(req.body);
	Post.findByIdAndUpdate(req.params.id,
		{'$push':
			{'comments':comment},
			
		},{upsert:true}, function(err, data){
			if (err){
				return console.log(err);
			}
			if(data ===null){
				return res.send(404);
			}
			return res.json(data);
		});
});


/*DELETE a comment nested in a post by id*/
router.delete('/:idParent/:idComment', function(req, res, next) {
	Post.findByIdAndUpdate(req.params.idParent,
		{'$pull':
			{'comments':{_id:req.params.idComment}}
	}, function(err,data){
		if (err){
			return console.log(err);
		}
		res.send({
			success: true,
			message: "Comment with id: "+ req.params.idComment+ "succesfully deleted!"}
		);
	});
});
module.exports = router;

