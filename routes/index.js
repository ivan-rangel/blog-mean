var express = require('express');
var router = express.Router();
var Post = require('../models/post');

/* GET home page. */
router.get('/', function(req, res, next) {
	Post.find({},function(err, posts){
		if (err)
			return console.log(err);
		res.render('index');

	});
});

/*New post endpoint*/
router.get('/new_post', function(req, res, next){
	res.render('new_post');
});

router.post('/create_post', function(req, res, next){
	var post = new Post({
		title: req.body.title,
		author: req.body.author,
		description: req.body.description
	});
	
	post.save(function(err){
		if (err)
			console.log(err);
		res.redirect('/');
	});


});


module.exports = router;
