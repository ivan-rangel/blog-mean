var app = angular.module('blog-mean', []);

app.controller('MainCtrl',mainCtrl);

function mainCtrl($scope, $http, $window, $location){
	var path = $location.path();
	$scope.isClicked = false;
	

	$scope.formData = {};

	/*Get all posts*/
	$http.get(path+'/api/posts').success(function(data){
		$scope.posts = data;
	});

	/*Create a new post*/
	$scope.createNewPost = function(){
		$http.post(path+'/create_post', $scope.formData)
			.success(function(data){
				$scope.formData = {};
				$window.location.href = '/';
				alert("New post created");
			})
			.error(function(){
				console.log('error');
			});
	};

	/*Delete a post*/
	$scope.deletePost = function(id){
		$scope.id = id;
		$http.delete(path+'/api/posts/'+$scope.id).
			success(function(data){
				$window.location.href = '/';
				alert('Post deleted');
			});
	};

	/*Add comments*/
	$scope.addComment = function(id){
		$scope.id = id;
		$http.patch(path+'/api/posts/'+$scope.id, $scope.formData)
			.success(function(data){
				alert('New comment added');
				$scope.formData={};
				$window.location.href = '/';
			})
			.error(function(){
				console.log('error');
			});
	};

	/*Delete comment*/
	$scope.deleteComment=function(idParent, idComment){
		$scope.idParent = idParent;
		$scope.idComment = idComment;
		console.log($scope.idParent, $scope.idComment);
		$http.delete('http://ec2-54-227-93-130.compute-1.amazonaws.com:3000/api/posts/'+$scope.idParent+'/'+$scope.idComment)
			.success(function(){
				alert('Comment deleted ');
				$window.location.href = '/';
			});
	};
}

